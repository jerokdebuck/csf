Darian Hadjiabadi
dhadjia1@gmail.com 
Computer System Fundamentals Assignment 1


All circuits have been drawn and are in Circuits.pdf. I apologize
if the drawings are hard to read, and I will try to improve the quality
of scans in future assignments.

Question 1

  a) AND using NAND

     Truth table for NAND

	io	i1	|	q
	==========
	0	0	|	1
	1	0	|	1
	0	1	|	1
	1	1	|	0
    
     Note that NAND = !AND so in order to obtain AND from    			
     NAND gates, negate NAND s.t   !NAND = !(!(AND)) = AND

     This can be done by taking input A and input B through a NAND gate then using the output as the inputs for a 
     second NAND gate.

  b) OR using NAND

   Truth table for OR
  
    Note that !A * !B = !(A+B). Then by negating !(A+B) (i.e. !(!(A+B)))
    one gets the OR gate. Also note (X*X)=X. Final form: ![!(A*A) * !(B*B)]

    The circuit can be completed such that input A and input B are each put through a seperate NAND gate. The outputs
     are then used as the inputs for a third NAND gate. 


  c) NOT using NAND

  Truth table for not
  
  io | q
  ======
  1  | 0 
  0  | 1    
 Note that !(X * X) = !X
  
 Therefore use A as the inputs for a NAND gate


d) AND using NOR

Truth table for NOR

io i1 | q
=======
0  0 | 1
1  0 | 0
0  1 | 0
1  1 | 0

Note that A*B = !(!A+!B) where !A + !B can be rewritten as !(A*B) by deMorgan's Laws. 

The circuit will have input A and input B go through their own seperate NOR gate. The output from each of these 
NOR gates is then put through a third NOR gate. 

e) OR using NOR

(A+B) = !(!(A+B))
Take inputs A and B through a NOR gate. This output will then be used as the two inputs to a second NOR gate. 

f) NOT using NOR
!A = !(A+A)
Take an input A and feed it as the two inputs to a NOR gate


Question 2

a  b |  a XOR b			a  b  | a XNOR  b
-----------------------			------- |-----------------
0  0 |  0 					0  0	  |	1
0  1 |  1					0  1	  |	0
1  0 |  1					1  0	  |	0
1  1 |  0					1  1	  |	1


a) Implementing XOR
Let q = a XOR b

q = (!a+!b) * (a + b)
q = !(a*b) * (a + b) (Simplified using deMorgan's Laws)

This circuit will contain four gates, 2 AND, 1 NOT, and 1 OR. Inputs A and B are put through an AND gate which is then inverted using a 
NOT gate. The output to this is one of the inputs for a second AND gate. In addtion, A and B are put through an OR gate, and this output
is the second input for the second AND gate. The resulting output is q.


 b) Implementing XNOR
Let q' = a XNOR b

q' = (!a*!b) + (a*b)
q' = !(a+b) + (a*b)  (Simplified using deMorgan's Laws)

This circuit can also be built out of four gates, 2 OR, 1 NOT, and 1 AND. Input A and B are first put through an OR gate, and the output of this
is inverted using a NOT gate. The output to this NOT gate is used as one of the inputs for a second OR gate. In addition, A and B are put through 
an AND gate. The output of this is now the second input for the second OR gate. The resulting output is q'.


Question 3


Truth Table for Half Adder

A  B   |   S   C
==========
0   0   |	0   0
0   1   |	1   0
1   0   |	1   0 
1   1   |	0   1


S = (!A * B) + (A * !B)
C = (A*B)

Note that in DNF, XOR has the following formula : (!A*B)+(A*!B). 

To obtain output S, simply use a XOR gate between inputs A and B. For output C, use an AND gate, which takes inputs A and B.


Question 4




	Truth table
	
	X	Y	Z	|	A	B	C	D	E	F	G
	=========|=======================
	0	0	0	|	1	1	1	1	1	1	0							
	0	0	1	|	0	1	1	0	0	0	0
	0	1	0	|	1	1	0	1	1	0	1
	0	1	1	|	1	1	1	1	0	0	1
	1	0	0	|	0	1	1	0	0	1	1
	1	0	1	|	1	0	1	1	0	1	1
	1	1	0	|	1	0	1	1	1	1	1
	1	1	1	|	1	1	1	0	0	0	0


	A = (!X+!Y+Z)*(X+!Y+!Z)
	B =  (X+!Y+Z)*(X+Y+!Z)
	C = (X+!Y+Z)
	D = (!X+!Y+Z) * (X+!Y+!Z) * (X+Y+Z)
	E = (!X*!Y*!Z) + (!X*Y*!Z) + (X*Y*!Z)
	F = (!X*!Y*!Z) + (X*!Y*!Z) + (X*!Y*Z) + (X*Y*!Z)
	G = (!X+!Y+!Z) * (X+Y+!Z) * (X+Y+Z)

	From the CNF and DNF forms above, a decoder circuit can be modeled. 
	For every !("X"), use a not gate. For every "*", use an AND gate. For every "+", use an OR gate.
	One can design the circuit with three input OR/AND gates or, using associativty property, using two input
	AND/OR gates and having the output be the input for a second AND/OR gate. (i.e. (X+Y+Z) = (X+Y) + Z)
	For instance, with A, (!X+!Y+Z) = (!X+!Y) + Z = !(X*Y) + Z. Take X and Y through an AND gate and put the 
	output through a NOT gate (or just use a NAND gate). The output of this will be the input to an OR gate where Z will be the second input.
	This can be done with all outputs in order to minimize truth table complexity if need be. 

	Please see attached PDF for design.
	The outputs are not really put into the proper positions based on the display. I tried
	my best to cram everything in so it all fit on one page. Apologies for that. This is just 
	one way to design such a circuit, I am sure there are at least five other ways to do so
	depending on if the user wants to minimize gates, complexity, etc. 