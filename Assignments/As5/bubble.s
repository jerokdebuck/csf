# bubble sort an array read from stdin to stdout
#
# we read up to 128 positive integers from stdin;
# if there are fewer than 128 integers, a negative
# integer can be used to mark the end of the input;
# we sort the array using bubble sort and finally
# print the sorted array back to stdout

sys_print_int = 1
sys_print_string = 4
sys_read_int = 5

SLOTS = 128	# at most 128 values
BYTES = 512	# need 128*4 bytes of space

	.text
main:
	sub	$sp, 4
	sw	$ra, 0($sp)

	li	$a0, SLOTS
	la	$a1, buffer
	jal	read_array
	ble	$v0, $zero, exit
	sw	$v0, used

	lw	$a0, used
	la	$a1, buffer
	jal	bubble_sort

	lw	$a0, used
	la	$a1, buffer
	jal	print_array
exit:
	lw	$ra, 0($sp)
	add	$sp, 4

	jr	$ra

# length = read_array(size, address)
# $v0                 $a0   $a1
read_array:
	sub	$sp, 16			# save used registers
	sw	$s2, 12($sp)
	sw	$s1, 8($sp)
	sw	$s0, 4($sp)
	sw	$ra, 0($sp)

	move	$s0, $zero # slots used
	move	$s1, $a1 # address in array
	move	$s2, $a0 # slots available
ra_loop:
	li	$v0, sys_read_int	# read next value
	syscall

	blt	$v0, $zero, ra_done	# break if we read < 0

	sw	$v0, ($s1)		# store value in array
	add	$s0, 1			# used++
	add	$s1, 4			# address++

	blt	$s0, $s2, ra_loop	# while used < available
ra_done:
	move	$v0, $s0		# return slots used

	lw	$ra, 0($sp)		# restore used registers
	lw	$s0, 4($sp)
	lw	$s1, 8($sp)
	lw	$s2, 12($sp)
	add	$sp, 16

	jr	$ra			# return

# bubble_sort(size, address)
#             $a0   $a1
bubble_sort:
  sub $sp, 16
  sw $s2, 12($sp)
  sw $s1, 8($sp)
  sw $s0, 4($sp)
  sw $ra, 0($sp)
bubble_outer_loop:
  li $s2, 1  #load loop index
  move $s1, $a1   #load address of array to
  move $s0, $a0  #load size of array
  li $v0, 0 #boolean to continue or not
bubble_inner_loop:
  lw $a2, 0($s1) #load a[i] 
  lw $a3, 4($s1) #load a[i+1]
  jal swap
  addi $s1, 4 #increment array index by 4
  addi $s2, 1 #increment loop index by 1
  bgt $s0, $s2, bubble_inner_loop
  bgtz $v0 bubble_outer_loop #if true, do another pass
bubble_return:
  lw $ra, 0($sp) #clean up and return
  lw $s0, 4($sp)
  lw $s1, 8($sp)
  lw $s2, 12($sp)
  addi $sp, 16
	jr $ra
swap:
  sub $sp, 4
  sw $ra, ($sp)
  sub $t0, $a3, $a2
  bgez $t0, swap_exit
  li $v0, 1 # set to 1 since as array is not sorted
  sw $a3, ($s1)  # a[i+1] = a[i]
  sw $a2, 4($s1) # a[i] = a[i+1]
swap_exit:
  lw $ra, ($sp)
  addi $sp, 4
  jr $ra
# print_array(size, address)
#             $a0   $a1
print_array:
	sub	$sp, 16			# save used registers
	sw	$s2, 12($sp)
	sw	$s1, 8($sp)
	sw	$s0, 4($sp)
	sw	$ra, 0($sp)

	move	$s0, $zero		# current slot
	move	$s1, $a1		# address in array
	move	$s2, $a0		# slots used
pa_loop:
	bge	$s0, $s2, pa_done	# while current < used

	lw	$a0, ($s1)		# load value in array
	jal	print_int		# and print it

	add	$s0, 1			# current++
	add	$s1, 4			# address++

	b	pa_loop
pa_done:
	lw	$ra, 0($sp)		# restore used registers
	lw	$s0, 4($sp)
	lw	$s1, 8($sp)
	lw	$s2, 12($sp)
	add	$sp, 16
	jr	$ra			# return

# print_int(value)
#           $a0
print_int:
	li	$v0, sys_print_int
	syscall

	la	$a0, lf
	li	$v0, sys_print_string
	syscall

	jr	$ra

# print_string(string)
#              $a0
print_string:
	li	$v0, sys_print_string
	syscall

	la	$a0, lf
	li	$v0, sys_print_string
	syscall

	jr	$ra

	.data
buffer:
	.space	BYTES		# array of 4-byte integers
used:
	.word	0		# used slots in array
lf:
	.asciiz	"\n"

	.end
