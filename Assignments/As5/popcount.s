
#Darian Hadjiabadi
#dhadjia1@gmail.com
#dhadjia1
#Code for Assignment 5 Problem 4
.text
.globl main
zero = 48 # ascii value of 0 is 48
offset = 39   # if character, subtract again by 39 to get in range a-f
multiplysixteen = 4   # for use with left shift
main:
    sub, $sp, 4
    sw $ra, ($sp)
    jal read_command_line   # gets command line argument
    move $a0, $v0 # move string argument into $a0
    jal get_integer   # converts hexadecimal string into integer base 10
    move $a0, $v0
    jal get_number_high   # gets number of high bits, in range of [0,32]
    move $a0, $v0
    li $v0, 1
    syscall
    la $a0, newline
    li $v0, 4
    syscall
    lw $ra, ($sp)
    add $sp, 4
    jr $ra

read_command_line:    # gets command line argument, stored in MEM[$a0]
    sub, $sp, 4
    sw, $ra, ($sp)
    lw $v0, 4($a1)    # get first command line argument
    lw, $ra, ($sp)
    add, $sp, 4
    jr $ra

get_integer:
    sub $sp, 4
    sw $ra, ($sp)
    move $s0, $a0   # string to $s0
    li $t0, zero  
    li $a2, 0
get_int_cp:
    lb $a1, 0($s0)    # load next character
    beqz $a1, get_int_exit    # if null, clean up and exit
    sub $a1, $a1, $t0   # else subtract once again by ascii "0"
    bge $a1, 32, is_lower_case    # if remained is at least 32, it's a lower case hex digit
get_int_cp_two:
    sll $a2, $a2, multiplysixteen   # multiply $a2 by 16
    add $a2, $a2, $a1   # add into $a2 the value of the next character
    addi $s0, 1   # increment $s0 by 1
    j get_int_cp
get_int_exit:
    move $v0, $a2
    lw $ra, ($sp)
    add $sp, 4
    jr $ra
is_lower_case:
    sub $a1, $a1, offset    # subtract by offset to get lowercase letter
    j get_int_cp_two

get_number_high:
    sub, $sp, 4
    sw $ra, ($sp)
    move $s0, $a0   # argument integer
    li $s1, 0       # bit number
    li $s2, 0       # num high bits
    li $t1, 0x1
high_cp:
    move $t0, $s0
    srlv $t0, $t0, $s1    # right shift by value in $s1
    and  $t0, $t0, $t1    # mask first bit and see if it is 0 or 1
    beq $t0, $t1, add_one # if 1, increment number of high bits by 1
add_one_return:
    addi $s1, 1   # increment number of bits to right shift next round by 1
    bne $s1, 32, high_cp    # if not 32, perform operations above again
    move $v0, $s2
    lw $ra, ($sp)
    add, $sp, 4
    jr $ra
add_one:
    addi $s2, 1
    j add_one_return 
.data
newline:
    .asciiz "\n"
    
    
