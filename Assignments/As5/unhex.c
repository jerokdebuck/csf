/* 
Darian Hadjiabadi
dhadjia1@gmail.com
dhadjia
Assignment 5 question 2 Solution
*/
#include <stdio.h>
#include <stdlib.h>
#define MAXHEXDIGITS 8
#define RIGHTSHIFT 28
#define LEFTSHIFT 4
#define UPPERHALFMASK 0xffff0000
#define LOWERHALFMASK 0x0000ffff
#define LOWERFOURMASK 0x0000000f
char * unhex(unsigned int);
int main(void) {
    unsigned int val[] = {0, -1, 65535, -65535, 10};
    unsigned int i;
    for (i = 0; i < sizeof(val) / sizeof(val[0]); i++) {
        char * hexStr = unhex(val[i]);
        printf("%d to hex is: %s\n", val[i], hexStr);
        free(hexStr);
    }
    return 0;
}
char * unhex(unsigned int value) {
    char * hexStr = calloc(sizeof(char), MAXHEXDIGITS);
    char digits[] = "0123456789abcdef";
    int loop = MAXHEXDIGITS;
    int buffer = 0;
    while (loop > 0) {
        unsigned int higherTemp = value & UPPERHALFMASK;    // next two lines of code are li    
        unsigned int lowerTemp = higherTemp | (value & LOWERHALFMASK);
        higherTemp = lowerTemp >> RIGHTSHIFT;   // next three lines of code are rol
        unsigned int temp1 = lowerTemp << LEFTSHIFT;
        temp1 |= higherTemp;
        value = temp1;
        temp1 &= LOWERFOURMASK;   // set temp1 to be in range [0,15]
        *(hexStr + buffer) = digits[temp1];
        loop -= 1;
        buffer += 1;
    }
    return hexStr;
}

/*
Note: it was importatnt to know the actual operations that made up pseudocode li and rol 
*/
