# John Lee; jlee405@jhu.edu; jlee405
# Darian Hadjiabadi; dhadjia1@gmail.com; dhadjia1

import sys
import time
import math
import os.path


def readFile(traceFile):
    lineCount = 0
    lines = []
    for line in traceFile:
        lineSplit = line.split()
        if len(lineSplit) == 0:
            continue
        elif (len(lineSplit) != 3):
            fileErrorHandle(0, lineCount)
        storeLoad = lineSplit[0]
        if (storeLoad != 'l' and storeLoad != 's'):
            fileErrorHandle(1, lineCount)
        hexAddress = lineSplit[1]
        if ('0x' not in hexAddress or '-' in hexAddress):
            fileErrorHandle(2, lineCount)
        lines.append((storeLoad, hexAddress))
        lineCount += 1
    return lines


def fileErrorHandle(number, lineCount):
    if (number == 0):
        sys.exit("Error! Incorrect number of variables in line: {0}".format(lineCount))
    elif (number == 1):
        sys.exit("Error! Field one is not an 'l' or 's', found in line: {0}".format(lineCount))
    elif (number == 2):
        sys.exit("Error! Memory address is incorrect format, found in line: {0}".format(lineCount))


def argumentErrorHandle():
    argmentList = []
    if (len(sys.argv) != 8):
        sys.exit("Error! Incorrect number of command line arguments")
    elif (int(sys.argv[1]) <= 0):
        sys.exit("Error! Incorrect argument for number of sets in each cache. Value must be > 0")
    elif (int(sys.argv[2]) <= 0):
        sys.exit("Error! Incorrect argument for number of blocks per set. Value must be > 0")
    elif (int(sys.argv[3]) <= 0):
        sys.exit("Error! Incorrect argument for number of bytes per block. Value must be > 0")
    elif (int(sys.argv[4]) != 0 and int(sys.argv[4]) != 1):
        sys.exit("Error! Argument 5 much be a 0 or 1")
    elif (int(sys.argv[5]) != 0 and int(sys.argv[5]) != 1):
        sys.exit("Error! Argument 6 much be a 0 or 1")
    elif (int(sys.argv[6]) != 0 and int(sys.argv[6]) != 1):
        sys.exit("Error! Argument 7 much be a 0 or 1")


def createCache(S, E, B, m):
    cache = [0] * S
    cache = [{} for i in cache]
    return cache


def parseAddress(address, tag_bits, set_index_bits, offset_bits, memory_bits):
    tag = int(address[0: tag_bits], 2)
    if set_index_bits > 0:
        set_index = int(address[tag_bits: tag_bits + set_index_bits], 2)
    else:
        set_index = 0
    byte_offset = int(address[memory_bits - offset_bits: memory_bits], 2)
    return (tag, set_index, byte_offset)


def simulateCache(cache, lines, sets, blocks_in_set, bytes_in_block,
    memory_bits, write_allocate, write_through, least_recently_used):
    ### Initialize bit sizes ###
    set_index_bits = int(math.log(sets, 2))  # s
    offset_bits = int(math.log(bytes_in_block, 2))  # b
    tag_bits = memory_bits - (set_index_bits + offset_bits)  # t = m -(s+b)

    ### Initialize stats ###
    loadCount, storeCount = 0, 0
    loadHits, storeHits = 0, 0
    loadMisses, storeMisses = 0, 0
    cycles = 0  # current number of elapsed cycles

    miss_penalty = 100*(bytes_in_block/4)

    #### Simulate all operations on cache ####
    for line in lines:
        address = bin(int(line[1], 16))[2:]
        address = '0'*(memory_bits - len(address)) + address
        parsed = parseAddress(address,
            tag_bits, set_index_bits, offset_bits, memory_bits)
        tag, set_index, byte_offset = parsed[0], parsed[1], parsed[2]
        storeLoad = line[0]
        tags = cache[set_index].keys()
        #last_access = time.time()
        ### load address ###
        if storeLoad == 'l':
            loadCount += 1
            # check valid bit
            if tag in tags:
                loadHits += 1
                #print '**Load Hit!**'
                cycles += 1
                last_access = cycles
                cache[set_index][tag][1] = last_access
            else:
                loadMisses += 1
                #print '**Load Miss...**'
                cycles += miss_penalty
                if len(tags) >= blocks_in_set:
                    dirty_bit = evict(cache, set_index, least_recently_used)
                    if dirty_bit:
                        cycles += miss_penalty
                dirty_bit = False
                cycles += 1
                first_access, last_access = cycles, cycles
                cache[set_index][tag] = [first_access, last_access, dirty_bit, line[1]]
        ### store address ###
        elif storeLoad == 's':
            storeCount += 1
            ## check valid bit ##
            if tag in tags:
                storeHits += 1
                #print '**Store Hit!**'
                cycles += 1
                last_access = cycles
                cache[set_index][tag][1] = last_access
                if write_through:
                    cycles += miss_penalty
                else:
                    dirty_bit = True
                    cache[set_index][tag][2] = dirty_bit
            else:
                storeMisses += 1
                #print '**Store Miss...**'
                cycles += miss_penalty
                if write_allocate:
                    if len(tags) >= blocks_in_set:
                        dirty_bit = evict(cache, set_index, least_recently_used)
                        if dirty_bit:
                            cycles += miss_penalty
                    dirty_bit = False
                    cycles += 1
                    first_access, last_access = cycles, cycles
                    cache[set_index][tag] = [first_access, last_access, dirty_bit, line[1]]
        #print '\t' + str((line[1], tag, set_index, byte_offset))
        #if tag in cache[set_index].keys():
            #print '\t'+ str(cache[set_index][tag])
    return [loadCount, storeCount, loadHits, loadMisses, storeHits, storeMisses, cycles]


def evict(cache, set_index, least_recently_used):
    if least_recently_used:
        worst_tag = min(cache[set_index].keys(),
            key=lambda x: cache[set_index][x][1])
    else:
        worst_tag = min(cache[set_index].keys(),
            key=lambda x: cache[set_index][x][0])
    #print '\tevicting: '
    #print '\t\t'+str(cache[set_index][worst_tag])
    dirty_bit = cache[set_index][worst_tag][2]
    del cache[set_index][worst_tag]
    return dirty_bit


def printResults(results):
    print 'Total Loads: ' + str(results[0])
    print 'Total Stores: ' + str(results[1])
    print 'Load hits: ' + str(results[2])
    print 'Load misses: ' + str(results[3])
    print 'Store hits: ' + str(results[4])
    print 'Store misses: ' + str(results[5])
    print 'Total cycles: ' + str(results[6])


def printParameters(sets, blocks_in_set, bytes_in_block, memory_bits,
    write_allocate, write_through, least_recently_used):
    print 'Sets: ' + str(sets)
    print 'Blocks in set: ' + str(blocks_in_set)
    print 'Bytes in block: ' + str(bytes_in_block) + '\n'
    set_index_bits = int(math.log(sets, 2))  # s
    offset_bits = int(math.log(bytes_in_block, 2))  # b
    tag_bits = memory_bits - (set_index_bits + offset_bits)  # t = m -(s+b)
    print 'Memory bits: ' + str(memory_bits)
    print 'Tag bits: ' + str(tag_bits)
    print 'Set index bits: ' + str(set_index_bits)
    print 'Offset bits: ' + str(offset_bits) + '\n'
    if write_allocate:
        print 'Write hit policy: write-allocate'
    else:
        print 'Write hit policy: no-write-allocate'
    if write_through:
        print 'Write miss policy: write-through'
    else:
        print 'Write miss policy: write-back'
    if least_recently_used:
        print 'Evict policy: least-recently-used'
    else:
        print 'Evict policy: FIFO'
    print


def main():
    argumentErrorHandle()
    traceFile = open(sys.argv[7], 'r')
    if (os.path.exists(sys.argv[7])):
        lines = readFile(traceFile)
    else:
        sys.exit("Error: File does not exist")
    # Set Indexing parameters
    sets = eval(sys.argv[1])  # S
    blocks_in_set = eval(sys.argv[2])  # E
    bytes_in_block = eval(sys.argv[3])  # B
    memory_bits = 32  # m
    # Set Write and Evict parameters
    write_allocate = bool(eval(sys.argv[4]))  # 1==write_allocate, 0==not
    write_through = bool(eval(sys.argv[5]))  # 1==write_through, 0==write_back
    least_recently_used = bool(eval(sys.argv[6]))  # 1==least_recently_used, 0==FIFO

    #Initialize cache
    cache = createCache(sets, blocks_in_set, bytes_in_block, memory_bits)

    #Simulate cache on lines of addresses
    results = simulateCache(cache, lines, sets, blocks_in_set, bytes_in_block,
        memory_bits, write_allocate, write_through, least_recently_used)

    #Print parameters
    #printParameters(sets, blocks_in_set, bytes_in_block, memory_bits,
    #    write_allocate, write_through, least_recently_used)

    #Print results
    printResults(results)
if __name__ == '__main__':
    main()
