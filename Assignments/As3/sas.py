#The assembler required for Assignment 2
#Darian Hadjiabadi
#dhadjia1@gmail.com
#dhadjia1
# To use: python sas.py < inputfile > outputfile

import sys
from array import array

#mapping from SCRAM instructions to bit pattern
encoding = {
    "HLT": 0b0000, "LDA": 0b0001, "LDI": 0b0010,
    "STA": 0b0011, "STI": 0b0100, "ADD": 0b0101,
    "SUB": 0b0110, "JMP": 0b0111, "JMZ": 0b1000,
}
# Total number of addresses allowed
MEM_SPACE = 16


# Searches to see if operation is recognized
def tableSearch(operation, count):
    if (operation == "HLT"):
        opVal = encoding[operation]
    elif (operation == "LDA"):
        opVal = encoding[operation]
    elif (operation == "LDI"):
        opVal = encoding[operation]
    elif (operation == "STA"):
        opVal = encoding[operation]
    elif (operation == "STI"):
        opVal = encoding[operation]
    elif (operation == "ADD"):
        opVal = encoding[operation]
    elif (operation == "SUB"):
        opVal = encoding[operation]
    elif (operation == "JMP"):
        opVal = encoding[operation]
    elif (operation == "JMZ"):
        opVal = encoding[operation]
    # If not found, return -1
    else:
        opVal = -1
    return opVal


# Obtain the upper 4 binary digits of the binary string
def upperBinary(opVal):
    opBin = bin(int(opVal))[2:]
    opBin = (4 - len(opBin)) * '0' + opBin
    return opBin


# Obtain the lower 4 binary digits of the string
def lowerBinary(operand):
    operandBinary = bin(operand)[2:]
    operandBinary = (4 - len(operandBinary)) * '0' + operandBinary
    return operandBinary


# Checks to make sure sure or address is in its
# correct range
def rangeHandle(operand, isInstruction, count):
    if (isInstruction == 1):
        if (operand > 15):
            errorHandle(count, 3)
        elif (operand < 0):
            errorHandle(count, 4)
    elif (isInstruction == 0):
        if (operand > 255):
            errorHandle(count, 5)
        elif (operand < 0):
            errorHandle(count, 6)


# Handles all the errors and exits program
# pep8 said lines here too long but I ignored it
def errorHandle(lineCount, errorNum):
    if (errorNum == 0):
        sys.exit("Error! Label defined twice in line: {0}".format(lineCount))
    elif (errorNum == 1):
        sys.exit("Error! Incorrect format in line: {0}".format(lineCount))
    elif (errorNum == 2):
        sys.exit("Error! Label not initialized, line: {0}".format(lineCount))
    elif (errorNum == 3):
        sys.exit("Error! address not in range[0,15], found in line: {0}".format(lineCount))
    elif (errorNum == 4):
        sys.exit("Error! Index of address is less than 0, found in line: {0}".format(lineCount))
    elif (errorNum == 5):
        sys.exit("Error! Value of Data is not in valid range of [0,255], found in line: {0}".format(lineCount))
    elif (errorNum == 6):
        sys.exit("Error! Value of Data is less than 0, found in line {0}".format(lineCount))
    elif (errorNum == 7):
        sys.exit("Error! Instruction could not be located, found in line: {0}".format(lineCount))
    elif (errorNum == 8):
        sys.exit("Error! Memory overflow starting at line: {0}".format(lineCount))
    elif (errorNum == 9):
        sys.exit("Error! 'start' must be declared only on first instruction, line: {0}".format(lineCount))


# Passes through the file, declaring new labels
# Also handels file formatting and returns errors if the
# code is not in proper format. The first pass is split
# up into three parts: A single argument, two arguements
# and three arguements - with logical based decisions
# based accordingly
def passOne(instructionFile):
    instructionNum = []
    dataNum = []
    numInstructions = 0
    numLine = 0
    justLabel = 0
    # to make sure that if line
    # contains just a label, there is operation/data
    # underneath it
    for line in instructionFile:
        line = line.strip()
        if (line == "HLT"):
            numLine += 1
            numInstructions += 1
            continue
        if '#' in line:
      #Get rid of comments, if there is an empty line, then move on.
            index = line.index('#')
            line = line[:index]
      # Skip blank lines
        if (line == ''):
            numLine += 1
            continue
        preSplit = line.split()
        # Split the line into a list
        if (len(preSplit) == 3):
        # Deals with lines with 3 arguements
            numLine += 1
            if (':' in preSplit[0]):
                justLabel = 0
                if (preSplit[1] == "DAT"):
                    preSplit[1] = "HLT"
                newIndex = preSplit[0].index(':')
        # Gets rid of ':' in all label initializations
                preSplit[0] = preSplit[0][: newIndex]
                for i in range(len(dataNum)):
                # Check to see if lable defined already
                    if (preSplit[0] == dataNum[i]):
                        errorHandle(numLine, 0)
     # Append the label name and its address
                instructionNum.append(numInstructions)
                dataNum.append(preSplit[0])
                numInstructions += 1
    # Increment the number of instructions by one
            else:
        #If an ':' is not found, then it is not a label definition and hence not in the correct format
                errorHandle(numLine, 1)
        elif (len(preSplit) == 2):  # Deals with lines of 2 arguements - doesn't really do much
            numInstructions += 1
            numLine += 1
            justLabel = 0
        elif (len(preSplit) == 1):  # Deals with lines of 1 arguement
            if (':' in preSplit[0]):
            # If line is just a label, add it
            # and move on
                index = preSplit[0].index(':')
                preSplit[0] = preSplit[0][:index]
                dataNum.append(preSplit[0])
                instructionNum.append(numInstructions)
                justLabel = 1
                numLine += 1
                continue
            else:
            # Is not an operation or a label
                numLine += 1
                errorHandle(numLine, 1)
        elif (len(preSplit) > 3):  # For lines with 4+ arguements
            numLine += 1
            errorHandle(numLine, 1)
    passTwo(instructionFile, instructionNum, dataNum)
# The second pass solidifies used labels get their
# respective addresses, builds a string of 8 bits
# representing the operation and operand, then flushes
# it out to standard output


def passTwo(instructionFile, instructionNum, dataNum):
    global MEM_SPACE
    memUsed = 0
  # keeps track of how much memory has been used. Each operation increments by 1
    lineCount = 0
  # keeps track of which line the parser is currently at
    bitField = array("B")
  # An array than contains integer values
    for line in instructionFile:
        line = line.strip()
        if '#' in line:
            index = line.index('#')
            line = line[:index]
        if (line == ''):
            lineCount += 1
            continue
        split = line.split()
        if (len(split) == 1):
            if (split[0] == "HLT"):
                lineCount += 1
                split.append('0')
            else:
                lineCount += 1
                continue  # Remove the first word in the line
        elif (len(split) == 3):
            split.pop(0)
        isInstruction = 1
        foundLabel = 0
    # See if address is an integer, if not search dataNum to see if label had been previously assigned
    # If so, give it value from instructionNum, which contains the address of the label
        lineCount += 1
        if (not split[1].isdigit()):
            if ('-' in split[1]):
            # Had to fill up this space so I added
            # a rather useless bit of code
                index = split[1].index('-')
            else:
                for i in range(len(dataNum)):
                    if (split[1] == dataNum[i]):
                        foundLabel = 1
                        split[1] = instructionNum[i]
                if (foundLabel == 0):
                    errorHandle(lineCount, 2)
        operation = split[0]
        operand = int(split[1])
        if (operation == "DAT"):
            isInstruction = 0
            rangeHandle(operand, isInstruction, lineCount)
            operation = "HLT"
        opVal = tableSearch(operation, lineCount)
        # Get the operation bits
        if (opVal == -1):
            errorHandle(lineCount, 7)
        instruction = getInstruction(opVal, operand, isInstruction, lineCount)
        bitField.append(int(instruction, 2))
        # Make sure appending with integer in base
        memUsed += 1
        if (memUsed > MEM_SPACE):
            errorHandle(lineCount, 8)
    bitField.tofile(sys.stdout)
    # Writes to file the machine code found in bitField


# Gets the 8 bit string code, then returns it
def getInstruction(opVal, operand, isInstruction, lineCount):
    operationBinary = upperBinary(opVal)
    rangeHandle(operand, isInstruction, lineCount)
    operandBinary = lowerBinary(operand)
    instruction = operationBinary + operandBinary
    return instruction


def main():
    #if (sys.platform.startswith('linux')):
    #  print("Good choice Mr Grader man")
    #else:
    #  print("Oh lordy")
    intructionFile = sys.stdin.readlines()
    passOne(intructionFile)

if __name__ == "__main__":
    main()
