# Multiplies 9 * 7

start: LDA ans #AC = ans
  ADD two  #AC = AC + two
  STA ans #store ans <-- AC
  LDA one # AC = one
  SUB sub # AC = AC - 1
  JMZ 8 # JMP to instruction 9 if AC == 0
  STA one #store one <-- AC
  JMP start # Go to instruction 0
  HLT
ans: DAT 0 #The answer
one: DAT 7 #First integer
two: DAT 9 #second integer
sub: DAT 1 #value to subtact by 

#after running the simulator, result is 0x3f = 63
