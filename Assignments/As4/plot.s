; Darian Hadjiabadi
; dhadjia1@gmail.com
; dhadjia1
; Plot subroutine created by Darian Hadjiabadi, rest by PHF
; a plot subroutine
; write the subroutine below to make the "random
; dots" animation work as shown in lecture

main:
	lda	$fe	; random
	and	#$1f	; 0 <= x <= 31
	sta	$00	; coordinate

	lda	$fe	; random
	and	#$1f	; 0 <= y <= 31
	sta	$01	; coordinate

	lda	$fe	; random
	and	#$0f	; 0 <= y <= 15
	sta	$02	; color

	jsr	plot	; plot the pixel

	jmp	main	; and again, forever

; draw a pixel at the coordinates and with the color
; given: x is in $00, y is in $01, color is in $02
plot:
; The idea of what I do in this first bit of the subroutine is multiply the x coordinate by 32. The reason for this is that an x-coordinate gives
; me what row to be on. Since the graphics is 32 bytes by 32 bytes, every 32 bytes I increment through will land me in the next row. So that is the
; first thing I did. Also since a random x-coordinate * 32 may go over 255, I had to allocate another memory slot $04 32 * x > 255. Then to finally
; get the final memory location, I added 32*x + y to get the number of bytes needed to traverse. 
		LDA $00	
		STA  $03	
		LDA #$00
		STA $04	
		ASL $03	; ASL multiplies by 2 so I need to do this 5 times
		ROL $04	; ROL will also shift all the bits in $04 to the left
		ASL $03
		ROL $04
		ASL $03
		ROL $04
		ASL $03
		ROL $04
		ASL $03
		ROL $04	; now number has been multiplied by two five times
		LDA $03
		ADC $01	; add y coordinate to 32 * x 
		BCS increment	; in case adding by y causes carry to be set, add one to memory slot $04
return:				; point of return from increment 
		STA $03	
		LDY #$00
		CPY $04			; do a bunch of comparisons here. Reason for this is that 
		BEQ zero			; depening on what memory location $04 holds it will detemine
		INY				; if i start on $0200, $0300, $0400, or $0500. 
		CPY $04	
		BEQ one
		INY
		CPY $04
		BEQ two 
		INY
		CPY $04
		BEQ three			; branch zero, one, two, three named after the value held
		increment: 		; in $04
			LDY $04
			INY
			STY $04
			JMP return 
		zero:
			LDA $02
			LDX $03	
			STA $0200, X	; store the value held in $02 to the correct memory location
			JMP end
		one: 
			LDA $02
			LDX $03
			STA $0300, X
			JMP end
		two:
			LDA $02
			LDX $03
			STA $0400, X
			JMP end
		three:
			LDA $02
			LDX $03
			STA $0500,X
			JMP end
		end: 
			rts