
; original program by PHF
; add32 and sub32 routines added by Darian Hadjiabadi
; dhadjia1@gmail.com
; dhadjia1

; 32 bit little endian addition and subtraction
;
; write the two subroutines below and make sure they
; compute the correct 32 bit little endian results

main:
	lda	#$12
	sta	$04
	sta	$05
	lda	#$13
	sta	$06
	sta	$07
	lda	#$3f
	sta	$00
	sta	$01
	sta	$02
	sta	$03
	jsr	add32
	jsr	sub32
	brk

; add the two 32 bit integers stored at $00..$03 and
; $04..$07 into a 32 bit result stored at $08..$0b
add32:
	PHA	; push accumulator into stack for good measure
	; set 'carry flag' off
	CLC
	 LDA $04	
         ADC $00 	; add lowest 8 bits
         STA $08
	 LDA $05
         ADC $01	; add second 8 bits
         STA $09
         LDA $06
         ADC $02	; add third 8 bits
         STA $0a
         LDA $07
         ADC $03	; add highest 8 bits
         STA $0b
         PLA
	 rts

; subtract the 32 bit integer stored at $04..$07 from
; the 32 bit integer stores at $00..$03 and put the
; 32 but result into $0c..$0f
sub32:
        PHA	; save accumulator for good measure
	; set 'carry flag' on, this is because subtraction subtracts with the not of the carry bit
	SEC 
	LDA $04
	SBC $00	; subtract lowest 8 bits
	STA $0c
	LDA $05
	SBC $01	; subtract 2nd set of 8 bits
	STA $0d
	LDA $06
	SBC $02	; subtract 3rd set of 8 bits
	STA $0e
	LDA $07
	SBC $03	; subtract highest 8 bits
	STA $0f
 	PLA
	rts
