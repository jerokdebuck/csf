; sort_array routine added by Darian Hadjiabadi
; dhadjia1@gmail.com
; dhadjia1
; animated array sorting in 6502 assembly
;
; write the subroutine below to sort the array in
; ascending order and make the animation work as
; shown in lecture; note that you don't have to
; write any graphics code, it's all there already
;
; zero page memory map
;
; $00/$01
;	pointer to current screen line for draw_array
; 	use ($00),y addressing to access pixel in line
; $10/$11
;	array indices to swap for swap_slot
; $20/$3f
;	array to be sorted

main:
	jsr	fill_array
	jsr	draw_array
	jsr	sort_array
	jsr  swap_slot
	brk

; sort the array, this is the code you have to write
; make sure you use swap_slot to exchange elements
; in the array, otherwise the animation won't work
; animated array sorting in 6502 assembly
;
; write the subroutine below to sort the array in
; ascending order and make the animation work as
; shown in lecture; note that you don't have to
; write any graphics code, it's all there already
;
; zero page memory map
;
; $00/$01
;	pointer to current screen line for draw_array
; 	use ($00),y addressing to access pixel in line
; $10/$11
;	array indices to swap for swap_slot
; $20/$3f
;	array to be sorted

main:
	jsr	fill_array
	jsr	draw_array
	jsr	sort_array
	jsr  swap_slot
	brk

; sort the array, this is the code you have to write
; make sure you use swap_slot to exchange elements
; in the array, otherwise the animation won't work
sort_array:
	lda #1
	sta $00  ; "find_min" loop index
	lda #0
	sta $01  ; minimum index
	lda #0
	sta $03	; "length_loop" index
	
length_loop:	; note that return_min and return_length are part of 
				; length_loop, I just divided it up into various checkpoints
	lda #31	; 31 = length - 1
	cmp $03	; see if this loop has iterated over the length of the array - 1
	bne find_min	; searches for the minimum value in the array
	jmp end
	return_min: 	; this checkpoint just compaes if min == a[i]. If no, swap. 
		lda $03
		cmp $05
		bne final_swap
	return_length:		; increment the outer loop index, and increment mins min and loop index
		ldx $03
		inx
		stx $03
		ldx $00
		inx
		stx $00
		ldx $01
		inx
        	stx $01
		jmp length_loop
final_swap:	; temp = a[min], a[min] = a[i], a[i] = temp
	ldx $05
	lda $20, X
	sta $04	; temp
	ldx $03
	lda $20, X
	ldx $05
	sta $20, X
	lda $04
	ldx $03
	sta $20, X
	jmp return_length
find_min:	; find the minimum value in the array
	lda $00
        sta $06  ; loop index
        lda $01
        sta $05  ; min	
	; $05 and $06 and temporary storage variables 
	post_create:	; a checkpoint for find_min
		lda #32	; 32 = length
		cmp $06	; see if min loop is done. If so return to return_min. If no, loop again.
		bne min_loop
		jmp return_min
min_loop:
	ldy $05		; min
	ldx $06	; index
	lda $20, X
	sta $02	; store the value found at $20 + index into $02, a temporary mem slot
	tya			; transfer min to accumulator and then to x
	tax
	lda $20, X	; put into accumulator the value at $20 + minimum
	sbc $02	; and subtract from the value at $20 + index
	bpl min_swap	; checks to see if a[i] < a[min]
	ldx $06	; increment index
	inx
	stx $06
	jmp post_create
min_swap:		; if a[i] < a[min], min = i
	lda $06
	sta $05	
	ldx $06
	inx
	stx $06
	jmp post_create
end: rts

; swap the two array slots indicated by $10 and $11
; and perform the necessary graphics updates for the
; sorting animation; would be more efficient to only
; draw the modified slots...
swap_slot:
	pha		; save registers
	txa
	pha
	tya
	pha

	ldx	$10	; first index
	lda	$20,x	; array[first]
	ldx	$11	; second index
	ldy	$20,x	; array[second]
	sta	$20,x	; array[second] = array[first]
	ldx	$10	; first index
	sty	$20,x	; array[first] = array[second]
	jsr	draw_array

	pla		; restore registers
	tay
	pla
	tax
	pla
	rts

; fill array with random values between 0 and 31
fill_array:
	ldx	#31	; count array index down from 31
fa_loop:
	lda	$fe	; grab a random byte
	and	#$1f	; force between 0 and 31
	sta	$20,x	; store in array[index]
	dex		; decrement index
	bpl	fa_loop	; another round if still positive

	rts

; draw the global array to the screen, Y coordinate
; is array position, X coordinate is value at that
; position
draw_array:
	; set up pointer to first screen line
	lda	#$02
	sta	$01
	lda	#$00
	sta	$00

	ldx	#31	; array index

da_next:
	; clear line
	ldy	#$1f
	lda	#0
da_clear:
	sta	($00),y
	dey
	bpl	da_clear
	; draw line
	ldy	$20,x	; array value at index
	lda	#3
da_pixel:
	sta	($00),y
	dey
	bpl	da_pixel

	; update pointer to next screen line
	lda	$00		; load low byte
	clc
	adc	#$20		; increment low byte
	sta	$00		; doesn't affect flags
	bcc	da_easy_line	; no overflow

	inc	$01		; increment high byte

da_easy_line:

	dex		; next array index
	bpl	da_next ; branch if negative flag clear

	rts


; swap the two array slots indicated by $10 and $11
; and perform the necessary graphics updates for the
; sorting animation; would be more efficient to only
; draw the modified slots...
swap_slot:
	pha		; save registers
	txa
	pha
	tya
	pha

	ldx	$10	; first index
	lda	$20,x	; array[first]
	ldx	$11	; second index
	ldy	$20,x	; array[second]
	sta	$20,x	; array[second] = array[first]
	ldx	$10	; first index
	sty	$20,x	; array[first] = array[second]
	jsr	draw_array

	pla		; restore registers
	tay
	pla
	tax
	pla
	rts

; fill array with random values between 0 and 31
fill_array:
	ldx	#31	; count array index down from 31
fa_loop:
	lda	$fe	; grab a random byte
	and	#$1f	; force between 0 and 31
	sta	$20,x	; store in array[index]
	dex		; decrement index
	bpl	fa_loop	; another round if still positive

	rts

; draw the global array to the screen, Y coordinate
; is array position, X coordinate is value at that
; position
draw_array:
	; set up pointer to first screen line
	lda	#$02
	sta	$01
	lda	#$00
	sta	$00

	ldx	#31	; array index

da_next:
	; clear line
	ldy	#$1f
	lda	#0
da_clear:
	sta	($00),y
	dey
	bpl	da_clear
	; draw line
	ldy	$20,x	; array value at index
	lda	#3
da_pixel:
	sta	($00),y
	dey
	bpl	da_pixel

	; update pointer to next screen line
	lda	$00		; load low byte
	clc
	adc	#$20		; increment low byte
	sta	$00		; doesn't affect flags
	bcc	da_easy_line	; no overflow

	inc	$01		; increment high byte

da_easy_line:

	dex		; next array index
	bpl	da_next ; branch if negative flag clear

	rts
