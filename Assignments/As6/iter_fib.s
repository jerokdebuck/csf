# Darian Hadjiabadi
# dhadjia1@gmail.com
# dhadjia1
# an iterative approach to finding the nth fibonnaci number

read_integer = 5
print_integer = 1
print_string = 4
  .text
main:
    sub $sp, 4
    sw $ra, ($sp)
    li $v0, read_integer
    syscall
    move $a0, $v0
    jal fib
    move $a0, $v0
    li $v0, print_integer
    syscall
    la $a0, new_line
    li $v0, print_string
    syscall
    lw $ra, ($sp)
    add $sp, 4
    jr $ra

fib:
    li $t0, 1   # f(n-2)
    li $t1, 0   # f(n-1)
    li $t2, 0   # f(n) 
    li $s0, 0   # counter
loop:
    beq $s0, $v0, done  # if counter = standard in, then we are done
    add $t2, $t1, $t0 # f(n) = f(n-1) + f(n-2)
    move $t0, $t1   # f(n-2) = f(n-1)
    move $t1, $t2   # f(n-1) = f(n)
    add $s0, 1      # increment counter by 1
    j loop
done:
    move $v0, $t2
    jr $ra

.data
new_line:
    .asciiz "\n"

