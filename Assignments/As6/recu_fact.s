# recursive factorial function, utter silliness of course

print_int = 1
print_string = 4
read_int = 5

	.text
main:
	sub	$sp, 4
	sw	$ra, 0($sp)

	li	$v0, read_int
	syscall

	move	$a0, $v0
	jal	factorial

	move	$a0, $v0
	li	$v0, print_int
	syscall

	la	$a0, lf
	li	$v0, print_string
	syscall

	lw	$ra, 0($sp)
	add	$sp, 4

	jr	$ra

# n!:$v0 = factorial(n:$a0)
factorial:
	sub	$sp, 8
	sw	$ra, 0($sp)
	sw	$s0, 4($sp)

	ble	$a0, 1, base

	move	$s0, $a0
	sub	$a0, 1
	jal	factorial
	mul	$v0, $v0, $s0

	b	done
base:
	li	$v0, 1
done:
	lw	$s0, 4($sp)
	lw	$ra, 0($sp)
	add	$sp, 8

	jr	$ra

	.data
lf:
	.asciiz	"\n"
