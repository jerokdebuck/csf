# Darian Hadjiabadi
# dhadjia1@gmail.com
# dhadjia1
# A recursive way to produce the nth fibonacci number

read_integer = 5
print_integer = 1
print_string = 4

  .text
main:
  sub $sp, 4
  sw $ra, 0($sp)

  li $v0, read_integer
  syscall
  move $a0, $v0  # counter / nth fib number to be found
  jal fib
  move $a0, $v0 #a0 = f(n)
  li $v0, print_integer
  syscall
  la $a0, new_line
  li $v0, print_string
  syscall
  
  lw $ra, 0($sp)
  add $sp, 4
  jr $ra

fib:
  sub $sp, 12
  sw $ra, 0($sp)
  sw $s0, 4($sp)
  sw $s1, 8($sp)
  beq $a0, 0, zero  # f(0) = 0
  beq $a0, 1, one   # f(1) = 1

  move $s0, $a0
  sub $a0, $s0, 1
  jal fib
  move $s1, $v0     # s1 = f(n-1)
  sub $a0, $s0, 2 
  jal fib           # v0 = f(n-2)
  add $v0, $v0, $s1   # v0 = f(n-2) + f(n-1)
  b done
zero:
    li $v0, 0
    j done
one:
    li $v0, 1
done:
    lw $s1, 8($sp)
    lw $s0, 4($sp)
    lw $ra, 0($sp)
    add $sp, 12
    jr $ra
 
  .data
new_line:
    .asciiz "\n"
  
