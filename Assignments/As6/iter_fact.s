# iterative factorial function

print_int = 1
print_string = 4
read_int = 5

	.text
main:
	sub	$sp, 4
	sw	$ra, ($sp)

	li	$v0, read_int
	syscall

	move	$a0, $v0
	jal	factorial

	move	$a0, $v0
	li	$v0, print_int
	syscall

	la	$a0, lf
  li	$v0, print_string
	syscall

	lw	$ra, ($sp)
	add	$sp, 4

	jr	$ra

# n!:$v0 = factorial(n:$a0)
factorial:
	li	$v0, 1		# result
loop:
	ble	$a0, 1, done

	mul	$v0, $v0, $a0
	sub	$a0, 1

	j	loop
done:
	jr	$ra

	.data
lf:
	.asciiz	"\n"
