# Darian Hadjiabadi
# dhadjia1@gmail.com
# dhadjia1
# bare memory access, branch and load delay slots
#
# The following program works fine when executed with
# the "regular" SPIM; it will print "333" as a result.
#
# We can tell SPIM to be a lot less nice to us, and
# for this problem that's what we'll do. Running SPIM
# with the "-bare" option gives us access only to the
# "bare" MIPS machine, none of the niceties that the
# assembler and simulator provide for us. In particular:
#
# - pseudo instruction such as li, la, and move will
#   no longer work; you have to perform the equivalent
#   operations using basic MIPS instructions such as
#   lui and addiu/ori
#
# - loads from memory will only take effect after
#   an additional instruction has been processed
#   (delayed loads)
#
# - branches and jumps will always execute the next
#   instruction after the branch as if it had been
#   written before the branch (delayed branches)
#
# - addressing memory with labels no longer works as
#   expected, instead you'll have to load the base
#   address of the .data segment (0x10000000) into a
#   register and then access memory indirectly with
#   an offset
#
# Modify the program (keeping the existing structure
# intact of course!) so that it will run without errors
# and produce the same result (333) when we run it with
# the "-bare" flag.

sys_print_int = 1
sys_print_string = 4
data_start = 0x1000
	.text
main:
	sub	$sp, $sp, 4
	sw	$ra, ($sp)
  #data running -bare starts at 0x10000000
  lui $at, data_start #load upper 16 bits into $at
  or  $at, $at, 0x0   #or with lower four bits

	lw	$t0, 0($at) # one located at address of $at
	lw	$t1, 4($at) # two located at address of $at + 4
  add $t2, $t0, 0 # due to delay in loading, this is sort of an useless step
	add	$t2, $t2, $t1
  add $a0, $t2, 0 # move $t2 to $a0 in order to print integer 
	jal	print_int
	lw	$ra, ($sp)
	add	$sp, $sp, 4
	jr	$ra

# print_int(i = $a0)
print_int:
  lui $v0, 0x0 
  ori $v0, $v0, sys_print_int
  syscall # print integer
  lui $a0, data_start
  ori $a0, $a0, 0x8
  lui $v0, 0x0
  ori $v0, $v0, sys_print_string
	jr	$ra
  syscall # delayed jump - print new line
	.data
one:
	.word	111
two:
	.word	222
lf:
	.asciiz	"\n"

	.end
