# Darian Hadjiabadi
# dhadjia1@gmail.com
# dhadjia1

# Calculates floor(sqrt(n)) using a digit-by-digit
# root calculation. I read up on this from wikpiedia to understand
# it. Originally, I had implemented Newton method but after 
# testing results I realized that the use of n / 2 was just a 
# guess and often times led to extra iterations resulting in ceil(sqrt(n)).

sys_print_integer = 1
sys_get_integer = 5
sys_print_string = 4

  .text
main:
  sub $sp, 4
  sw $ra, 0($sp)
  li $v0, sys_get_integer
  syscall
  move $a0, $v0
  jal sqrt
  move $a0, $v0
  li $v0, sys_print_integer
  syscall
  la $a0, new_line
  li $v0, sys_print_string
  syscall
  lw $ra, 0($sp)
  add $sp, 4
  jr $ra
sqrt:
  sub $sp, 8
  sw $ra, 0($sp)
  sw $s0, 4($sp)

  move $s0, $a0   # $s0 = argument number
  li $t0, 0       # return value 
  li $t1, 1       # bit
  sll $t1, $t1, 30    # bit =<< 30
bit_loop:
  slt $t2, $s0, $t1   # checks if arguement < bit
  beq $t2, $zero, sqrt_loop
  srl $t1, $t1, 2     # bit >>= 2
  j bit_loop
sqrt_loop:
  beq $t1, $zero, done    # sqrt_loop is essentially a while loop and runs so long as bit != 0
  add $t3, $t0, $t1   # t3 = return + bit
  slt $t2, $s0, $t3   # checks if argument < (return + bit)
  beq $t2, $zero, else  # if not, go to else
  srl $t0, $t0, 1       # return >>= 1
  j end_loop
else:
  sub $s0, $s0, $t3     # argument = argument - (bit + return)
  srl $t0, $t0, 1       # return >>= 1
  add $t0, $t0, $t1     # return = return + bit
end_loop:
  srl $t1, $t1, 2       # bit =<< 2
  j sqrt_loop
done:
  move $v0, $t0
  lw $s0, 4($sp)
  lw $ra, 0($sp)
  add $sp, 8
  jr $ra

  .data
new_line:
  .asciiz "\n"
